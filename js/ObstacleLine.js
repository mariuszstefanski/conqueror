class ObstacleLine extends GameObject{
 
    constructor(intervalTime){
        super(intervalTime); /* as this class extends from GameObject, you must always call super() */

        this.offscreenCanvas = document.createElement('canvas');
        this.offscreenCanvasCtx = this.offscreenCanvas.getContext('2d');
        
        this.offscreenCanvas.width = canvas.width;
        this.offscreenCanvas.height = canvas.height;

        /* These variables depend on the object */
        this.startX = 0;
        this.startY = 0;
        this.endX = 0;
        this.endY = 0;

        this.offscreenCanvasCtx.lineWidth = 10;


        // this.offscreenCanvasCtx.beginPath();
        // //ctx.lineWidth = "10";
        // this.offscreenCanvasCtx.rect(0, 0, canvas.width, canvas.height);
        // this.offscreenCanvasCtx.stroke();

        //this.setStartOfLine(this.startX, this.startY);
        // this.offscreenCanvasCtx.beginPath();
        // this.offscreenCanvasCtx.moveTo(this.startX, this.startY);
    
    }

    render(){
        ctx.drawImage(this.offscreenCanvas, 0 , 0, canvas.width, canvas.height);
    }

    setStartOfLine(x, y){
        this.startX = x;
        this.startY = y;
        this.offscreenCanvasCtx.beginPath();
        this.offscreenCanvasCtx.moveTo(x, y);
    }

    setEndOfLine(x, y){
        this.endX = x;
        this.endY = y;
        this.offscreenCanvasCtx.lineTo(x, y);
        this.offscreenCanvasCtx.stroke();
    }

    clearCanvas(){
        this.offscreenCanvasCtx.clearRect(0, 0, canvas.width, canvas.height);
    }

    enemyTouchedLine(x, y){
        this.startX = x;
        this.startY = y;
        this.endX = x;
        this.endY = y;
        this.offscreenCanvasCtx.beginPath();
        this.offscreenCanvasCtx.moveTo(this.startX, this.startY);
    }


    stopDrawing(){
        this.gameObjectIsDisplayed = false;
    }

    startDrawing(){
        this.gameObjectIsDisplayed = true;
    }


}