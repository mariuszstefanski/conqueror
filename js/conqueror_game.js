/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland.             */
/* There should always be a javaScript file with the same name as the html file. */
/* This file always holds the playGame function().                               */
/* It also holds game specific code, which will be different for each game       */

/******************** Declare game specific global data and functions *****************/
/* images must be declared as global, so that they will load before the game starts  */
let ballImage = new Image();
ballImage.src = "img/ball.png";

let enemyBallImage = new Image();
enemyBallImage.src = "img/enemy_ball.png";
/******************* END OF Declare game specific data and functions *****************/


/* Always have a playGame() function                                     */
/* However, the content of this function will be different for each game */
function playGame()
{
    /* We need to initialise the game objects outside of the Game class */
    /* This function does this initialisation.                          */
    /* Specifically, this function will:                                */
    /* 1. initialise the canvas and associated variables                */
    /* 2. create the various game gameObjects,                   */
    /* 3. store the gameObjects in an array                      */
    /* 4. create a new Game to display the gameObjects           */
    /* 5. start the Game                                                */



    /* Create the various gameObjects for this game. */
    /* This is game specific code. It will be different for each game, as each game will have it own gameObjects */

    const urlParams = new URLSearchParams(window.location.search);
    level = urlParams.get('level');
    email = urlParams.get('email');
    if (urlParams.get('highscore') != 'undefined'){
        highestScore = urlParams.get('highscore');
    } else{
        highestScore =0;
    }
    
    
    gameObjects[OBSTACLE_LINE] = new ObstacleLine(1);
    gameObjects[OBSTACLE_RECT] = new ObstacleRect(1);
    gameObjects[BALL] = new Ball(ballImage, 10, 10, 10, 10, 1, gameObjects[OBSTACLE_LINE], gameObjects[OBSTACLE_RECT]);
    gameObjects[ENEMY_BALL] = new EnemyBall(enemyBallImage, 10, 10, 1, 1);
    gameObjects[LIFE] = new Life(1);
    gameObjects[GAME_OVER_MESSAGE] = new GameOverMessage(1);
    gameObjects[SCORE] = new Score(1);
    gameObjects[YOU_WON_MESSAGE] = new YouWonMessage(1);
    //gameObjects[3] = new EnemyBall(enemyBallImage, 10, 10, 1, 1);

    /* END OF game specific code. */

    window.addEventListener("deviceorientation", handleOrientation, true);



    window.addEventListener("keydown", event => {
        if (event.isComposing || event.code === 'KeyW') {
            gameObjects[BALL].determineIfShoulMoveY(-1);
        }

        if (event.isComposing || event.code === 'KeyS') {
            gameObjects[BALL].determineIfShoulMoveY(1);
        }

        if (event.isComposing || event.code === 'KeyA') {
            gameObjects[BALL].determineIfShoulMoveX(-1);
        }

        if (event.isComposing || event.code === 'KeyD') {
            gameObjects[BALL].determineIfShoulMoveX(1);
        }
      });


    /* Always create a game that uses the gameObject array */
    let game = new ConquerorGame();

    /* Always play the game */
    game.start();


    /* If they are needed, then include any game-specific mouse and keyboard listners */
}

function handleOrientation(event) {  
    //beta+ dół, beta- góra
    //gamma+ prawo, gamma- lewo

    if(event.beta > event.gamma){
        gameObjects[BALL].determineIfShoulMoveY(event.beta);
    }
    else{
        gameObjects[BALL].determineIfShoulMoveX(event.gamma);
    }

  }