/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland. */

class Ball extends GameObject
{
    /* Each gameObject MUST have a constructor() and a render() method.        */
    /* If the object animates, then it must also have an updateState() method. */

    constructor(image, x, y,  width, height, intervalTime, obstacleLine, obstacleRect)
    {
        super(intervalTime); /* as this class extends from GameObject, you must always call super() */

        /* These variables depend on the object */
        this.image = image;
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y; 
        this.stepSizeX = 0;
        this.stepSizeY = 0;
       
        this.shouldMoveRight = false;
        this.shouldMoveLeft = false;
        this.shouldMoveUp = false;
        this.shouldMoveDown = false;


        this.obstacleOnLeft = false;
        this.obstacleOnRight = false;
        this.obstacleAtBottom = false;
        this.obstacleAtTop = false;


        this.halfHeight = height / 2;
        this.halfWidth = width / 2;

        this.obstacleLine = obstacleLine;
        this.obstacleRect = obstacleRect;

        this.isDrawingLine = true;
        this.moving = true;
        // this.obstacleLine.setStartOfLine(this.x+this.halfWidth, this.y+this.halfHeight);
    }

    determineIfShoulMoveX( stepSize ){
        if(stepSize > 0){
            this.shouldMoveRight = true;
            this.shouldMoveLeft = false;
        }
        else if ( stepSize < 0 ) {
            this.shouldMoveRight = false;
            this.shouldMoveLeft = true;
        } else {
            this.shouldMoveRight = false;
            this.shouldMoveLeft = false;
        }

        this.shouldMoveDown = false;
        this.shouldMoveUp = false;
   }

   determineIfShoulMoveY( stepSize ){
    if(stepSize > 0 ){
        this.shouldMoveDown = true;
        this.shouldMoveUp = false;
    }
    else if ( stepSize < 0 ) {
        this.shouldMoveDown = false;
        this.shouldMoveUp = true;
    } else if( stepSize == 0 ){
        this.shouldMoveDown = false;
        this.shouldMoveUp = false;
        }
        this.shouldMoveRight = false;
        this.shouldMoveLeft = false;
    }

    updateState(){ 

        if (!gameObjects[GAME_OVER_MESSAGE].gameOver){

            this.determinateMovement();
            this.obstacleCheck();

            let newX = this.x + this.stepSizeX;
            let newY = this.y + this.stepSizeY;

            if (this.isDrawingLine){    
                if(this.isMoving()){

                    this.obstacleLine.setEndOfLine(newX+this.halfWidth, newY+this.halfHeight);
                } else{
                    gameObjects[SCORE].updateScore();
                    this.obstacleRect.drawImage(this.obstacleLine.offscreenCanvas);
                    this.obstacleLine.clearCanvas();
                    this.obstacleLine.setStartOfLine(this.x+this.halfWidth, this.y+this.halfHeight);
                }
            }

            if(this.isDrawingLine && this.isMoving()){
                let xDifference = Math.abs(this.x - newX);
                let yDifference = Math.abs(this.y - newY);
                gameObjects[SCORE].valueBuffer += xDifference+yDifference;
            }

            if (this.moving){
                this.x = newX;
                this.y = newY;
            }
        }
        }
       

    render(){
        ctx.drawImage(this.image, this.x , this.y, this.width, this.height);
    }

    determinateMovement(){
        if(this.shouldMoveRight){
            this.stepSizeX = 1;
        }

        if(this.shouldMoveLeft){
            this.stepSizeX = -1;
        }

        if (!this.shouldMoveLeft && !this.shouldMoveRight ){
            this.stepSizeX = 0;
        } 

        if(this.shouldMoveDown){
            this.stepSizeY = 1;
        }

        if(this.shouldMoveUp){
            this.stepSizeY = -1;
        }

        if (!this.shouldMoveDown && !this.shouldMoveUp ){
            this.stepSizeY = 0;
        }
    }

   obstacleCheck(){
    if(this.obstacleOnRight && this.shouldMoveRight){
        this.stepSizeX = 0;
        gameObjects[SCORE].updateScore();
    } 

    if(this.obstacleOnLeft && this.shouldMoveLeft){
        this.stepSizeX = 0;
        gameObjects[SCORE].updateScore();
    }
    if(this.obstacleAtBottom && this.shouldMoveDown){
        this.stepSizeY = 0;
        gameObjects[SCORE].updateScore();
    }

    if(this.obstacleAtTop && this.shouldMoveUp){
        this.stepSizeY = 0;
        gameObjects[SCORE].updateScore();
    }

   }

   isMoving(){
       return this.stepSizeX !=0 || this.stepSizeY != 0;
   }

}