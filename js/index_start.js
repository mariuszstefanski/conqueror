window.onload=function(){
     document.getElementById("loginButton").addEventListener("click", getDataFromDatabase);
  }

var email = null;
var highestScores = [];
 
function getDataFromDatabase() {

    email = document.getElementById("email").value;

    document.getElementById('highestScoreLevel1').innerHTML = '';
    document.getElementById('highestScoreLevel2').innerHTML = '';
    document.getElementById('highestScoreLevel3').innerHTML = '';
    document.getElementById('highestScoreLevel4').innerHTML = '';
    document.getElementById('highestScoreLevel5').innerHTML = '';


    if(email != ""){
        var docRef = db.collection(highScoresCollection).doc(email);
    
        docRef.get().then(function(doc) {
            if (doc.exists) {
                document.getElementById("message").innerHTML = "Pobrano dane";

                if(doc.data().highestScoreLevel1){
                    highestScores[0] = doc.data().highestScoreLevel1;
                    document.getElementById('highestScoreLevel1').innerHTML = 'Highest score: '+doc.data().highestScoreLevel1;
                    document.getElementById('level2Button').disabled = false;
                }
                if(doc.data().highestScoreLevel2){
                    highestScores[1] = doc.data().highestScoreLevel2;
                    document.getElementById('highestScoreLevel2').innerHTML = 'Highest score: '+doc.data().highestScoreLevel2;
                    document.getElementById('level3Button').disabled = false;
                }
                if(doc.data().highestScoreLevel3){
                    highestScores[2] = doc.data().highestScoreLevel3;
                    document.getElementById('highestScoreLevel3').innerHTML = 'Highest score: '+doc.data().highestScoreLevel3;
                    document.getElementById('level4Button').disabled = false;
                }
                if(doc.data().highestScoreLevel4){
                    highestScores[3] = doc.data().highestScoreLevel4;
                    document.getElementById('highestScoreLevel4').innerHTML = 'Highest score: '+doc.data().highestScoreLevel4;
                    document.getElementById('level5Button').disabled = false;
                }

                if(doc.data().highestScoreLevel5){
                    highestScores[4] = doc.data().highestScoreLevel5;
                    document.getElementById('highestScoreLevel5').innerHTML = 'Highest score: '+doc.data().highestScoreLevel5;
                }



            } else {
                // doc.data() will be undefined in this case
                console.log("No such document!");
                document.getElementById("message").innerHTML = "Brak danych";
            }
        }).catch(function(error) {
            console.log("Error getting document:", error);
            document.getElementById("message").innerHTML = "Wystąpił błąd";
        });
    }


}

function goToGame(level){
    level = level;
    window.location.href="conqueror_game.html?level="+level+"&email="+email+"&highscore="+highestScores[level-1];
}