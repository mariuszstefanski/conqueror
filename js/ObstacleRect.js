class ObstacleRect extends GameObject {

    constructor(intervalTime){
        super(intervalTime); /* as this class extends from GameObject, you must always call super() */

        this.offscreenCanvas = document.createElement('canvas');
        this.offscreenCanvasCtx = this.offscreenCanvas.getContext('2d');
        
        this.offscreenCanvas.width = canvas.width;
        this.offscreenCanvas.height = canvas.height;

        //this.offscreenCanvasCtx.strokeStyle = "#FF0000";
        this.offscreenCanvasCtx.lineWidth = 10;

        this.drawRectangle(5,5, canvas.width-10, canvas.height-10);
        //this.drawLine(250, 0, 250, 500);
    }
    render(){
        ctx.drawImage(this.offscreenCanvas, 0 , 0, canvas.width, canvas.height);
    }

    drawImage(canvasSource){
        this.offscreenCanvasCtx.drawImage(canvasSource,0,0);
    }

    drawRectangle(startX, startY, width, height){
        this.offscreenCanvasCtx.beginPath();
        this.offscreenCanvasCtx.rect(startX, startY, width, height);
        this.offscreenCanvasCtx.stroke();
    }

    drawLine(startX, startY, endX, endY){
        this.offscreenCanvasCtx.beginPath();
        this.offscreenCanvasCtx.moveTo(startX, startY);
        this.offscreenCanvasCtx.lineTo(endX, endY);
        this.offscreenCanvasCtx.stroke();
    }

    
}