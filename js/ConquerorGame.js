class ConquerorGame extends CanvasGame {

    collisionDetection()    {  
        this.checkIfEnemyBallsCollidesWithObstacleRect();
        this.checkIfEnemyBallsCollidesWithObstacleLine(); 
        
        this.checkIfBallCollidesWithObstacle();
    }

    checkIfEnemyBallsCollidesWithObstacleRect(){
        let obstacleOnLeft = false;
        let obstacleOnRight = false;
        let obstacleAtBottom = false;
        let obstacleAtTop = false;

        let x = gameObjects[ENEMY_BALL].x;
        let y = gameObjects[ENEMY_BALL].y;
        let width = gameObjects[ENEMY_BALL].width;
        let height = gameObjects[ENEMY_BALL].height;

        //moving left
        if (gameObjects[ENEMY_BALL].stepSizeX == -1){
            let imageDataLeft = gameObjects[OBSTACLE_RECT].offscreenCanvasCtx.getImageData(x-1, y+(height/2), 1, 1).data;
            if(imageDataLeft[ALPHA] == 255){
                obstacleOnLeft = true;
            }
        }

        //moving right
        if (gameObjects[ENEMY_BALL].stepSizeX == 1){
            let imageDataRight = gameObjects[OBSTACLE_RECT].offscreenCanvasCtx.getImageData(x+width+1, y+(height/2), 1, 1).data;
            if(imageDataRight[ALPHA] == 255){
                obstacleOnRight = true;
            }
        }

        //moving down
        if (gameObjects[ENEMY_BALL].stepSizeY == 1){
            let imageDataDown = gameObjects[OBSTACLE_RECT].offscreenCanvasCtx.getImageData(x+(width/2), y+height+1, 1, 1).data;
            if(imageDataDown[ALPHA] == 255){
                obstacleAtBottom = true;
            }
        }

        //moving up
        if (gameObjects[ENEMY_BALL].stepSizeY == -1){
            let imageDataUp = gameObjects[OBSTACLE_RECT].offscreenCanvasCtx.getImageData(x+(width/2), y-1, 1, 1).data;
            if(imageDataUp[ALPHA] == 255){
                obstacleAtTop = true;
            }
        }

        if(obstacleOnLeft || obstacleOnRight){
            gameObjects[ENEMY_BALL].stepSizeX = -gameObjects[ENEMY_BALL].stepSizeX;
        }

        if(obstacleAtBottom || obstacleAtTop){
            gameObjects[ENEMY_BALL].stepSizeY = -gameObjects[ENEMY_BALL].stepSizeY;
        }

    }

    checkIfEnemyBallsCollidesWithObstacleLine(){
        let obstacleOnLeft = false;
        let obstacleOnRight = false;
        let obstacleAtBottom = false;
        let obstacleAtTop = false;

        let collides = false;

        let x = gameObjects[ENEMY_BALL].x;
        let y = gameObjects[ENEMY_BALL].y;
        let width = gameObjects[ENEMY_BALL].width;
        let height = gameObjects[ENEMY_BALL].height;

        //moving left
        if (gameObjects[ENEMY_BALL].stepSizeX == -1){
            let imageDataLeft = gameObjects[OBSTACLE_LINE].offscreenCanvasCtx.getImageData(x-1, y+(height/2), 1, 1).data;
            if(imageDataLeft[ALPHA] == 255){
                obstacleOnLeft = true;
                collides = true;
            }
        }

        //moving right
        if (gameObjects[ENEMY_BALL].stepSizeX == 1){
            let imageDataRight = gameObjects[OBSTACLE_LINE].offscreenCanvasCtx.getImageData(x+width+1, y+(height/2), 1, 1).data;
            if(imageDataRight[ALPHA] == 255){
                obstacleOnRight = true;
                collides = true;
            }
        }

        //moving down
        if (gameObjects[ENEMY_BALL].stepSizeY == 1){
            let imageDataDown = gameObjects[OBSTACLE_LINE].offscreenCanvasCtx.getImageData(x+(width/2), y+height+1, 1, 1).data;
            if(imageDataDown[ALPHA] == 255){
                obstacleAtBottom = true;
                collides = true;
            }
        }

        //moving up
        if (gameObjects[ENEMY_BALL].stepSizeY == -1){
            let imageDataUp = gameObjects[OBSTACLE_LINE].offscreenCanvasCtx.getImageData(x+(width/2), y-1, 1, 1).data;
            if(imageDataUp[ALPHA] == 255){
                obstacleAtTop = true;
                collides = true;
            }
        }

        if(collides){
            gameObjects[LIFE].removeLife();
            gameObjects[OBSTACLE_LINE].clearCanvas();
            gameObjects[OBSTACLE_LINE].enemyTouchedLine(gameObjects[BALL].x+5, gameObjects[BALL].y+5);
            gameObjects[BALL].isDrawingLine = false;
            gameObjects[SCORE].valueBuffer = 0;
        }
    }

    checkIfBallCollidesWithObstacle(){
        gameObjects[BALL].obstacleOnLeft = false;
        gameObjects[BALL].obstacleOnRight = false;
        gameObjects[BALL].obstacleAtBottom = false;
        gameObjects[BALL].obstacleAtTop = false;

        let x = gameObjects[BALL].x;
        let y = gameObjects[BALL].y;
        let width = gameObjects[BALL].width;
        let height = gameObjects[BALL].height;

        //left
        let imageDataLeftRect = gameObjects[OBSTACLE_RECT].offscreenCanvasCtx.getImageData(x-1, y+(height/2), 1, 1).data;
        let imageDataLeftLine = gameObjects[OBSTACLE_LINE].offscreenCanvasCtx.getImageData(x-1, y+(height/2), 1, 1).data;
        if(imageDataLeftLine[ALPHA] == 255 || imageDataLeftRect[ALPHA] == 255 ){
            gameObjects[BALL].obstacleOnLeft = true;
            gameObjects[BALL].isDrawingLine = true;
        }
    
        //right
        let imageDataRightLine = gameObjects[OBSTACLE_RECT].offscreenCanvasCtx.getImageData(x+width+1, y+(height/2), 1, 1).data;
        let imageDataRightRect = gameObjects[OBSTACLE_LINE].offscreenCanvasCtx.getImageData(x+width+1, y+(height/2), 1, 1).data;
        if(imageDataRightLine[ALPHA] == 255 || imageDataRightRect[ALPHA] == 255){
            gameObjects[BALL].obstacleOnRight = true;
            gameObjects[BALL].isDrawingLine = true;
        }
        
        //down
        let imageDataDownRect = gameObjects[OBSTACLE_RECT].offscreenCanvasCtx.getImageData(x+(width/2), y+height+1, 1, 1).data;
        let imageDataDownLine = gameObjects[OBSTACLE_LINE].offscreenCanvasCtx.getImageData(x+(width/2), y+height+1, 1, 1).data;
        if(imageDataDownLine[ALPHA] == 255 || imageDataDownRect[ALPHA] == 255){
            gameObjects[BALL].obstacleAtBottom = true;
            gameObjects[BALL].isDrawingLine = true;
        }     

        //up
        let imageDataUpRect = gameObjects[OBSTACLE_RECT].offscreenCanvasCtx.getImageData(x+(width/2), y-1, 1, 1).data;
        let imageDataUpLine = gameObjects[OBSTACLE_LINE].offscreenCanvasCtx.getImageData(x+(width/2), y-1, 1, 1).data;
        if(imageDataUpLine[ALPHA] == 255 || imageDataUpRect[ALPHA] == 255){
            gameObjects[BALL].obstacleAtTop = true;
            gameObjects[BALL].isDrawingLine = true;
        }
    }

}