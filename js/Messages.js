class GameOverMessage extends GameObject{

    constructor(intervalTime){
        super(intervalTime);
        this.gameOver = false;
        this.maxWidth = 0.5 * canvas.width;
        this.x = 0.5 * this.maxWidth;
        this.y = 0.5 * canvas.height;
    }

    show(){
        this.gameOver = true;


    }

    render(){
        if (this.gameOver){
            navigator.vibrate(500);
            ctx.strokeStyle = "red";
            ctx.font = "30px Arial";
            ctx.lineWidth = 3;
            // TODO zmienic wartości na zależne od canvas
            ctx.strokeText("GAME OVER", this.x, this.y, this.maxWidth);
        }
    } 
}

class YouWonMessage extends GameObject {

    constructor(intervalTime){
        super(intervalTime);
        this.won = false;
        this.maxWidth = 0.5 * canvas.width;
        this.x = 0.5 * this.maxWidth;
        this.y = 0.5 * canvas.height;
    }

    show(){
        this.won = true;
    }

    render(){
        if (this.won){
            navigator.vibrate(500);
            ctx.strokeStyle = "red";
            ctx.font = "30px Arial";
            ctx.lineWidth = 3;
            // TODO zmienic wartości na zależne od canvas
            ctx.strokeText("YOU WON", this.x, this.y, this.maxWidth);
        }
    } 
}



