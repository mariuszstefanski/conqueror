class Score extends GameObject{ 

    constructor(x, y, intervalTime){
        super(intervalTime)
        this.value = 0;
        this.valueBuffer = 0;
        this.x = 0.1 * canvas.width;
        this.y = 0.08 * canvas.height;
        this.scoreSend = false;
    }

    render(){
        ctx.strokeStyle = "blue";
        ctx.font = "30px Arial";
        ctx.lineWidth = 3;
        ctx.strokeText(this.value,  this.x, this.y);
    }

    add(){
        this.valueBuffer++;
    }

    updateScore(){
        this.value += this.valueBuffer;
        this.valueBuffer = 0;

        if(this.value >= 2000){
            gameObjects[YOU_WON_MESSAGE].show();
            gameObjects[ENEMY_BALL].moving = false;
            gameObjects[BALL].moving = false;

        
            
        if(email != 'undefined'){

            if (!this.scoreSend){
                var highScoresRef = db.collection(highScoresCollection).doc(email);


                        if(level == 1){
                            

                            if(this.value > highestScore){
                                highScoresRef.set({
                                    highestScoreLevel1 : this.value
                                }, { merge: true });
                            }

                        }

                        if(level == 2){
                          
                            if(this.value > highestScore){
                                highScoresRef.set({
                                    highestScoreLevel2 : this.value
                                }, { merge: true });
                            }
                        }

                        if(level == 3){
                          

                            if(this.value > highestScore){
                                highScoresRef.set({
                                    highestScoreLevel3 : this.value
                                }, { merge: true });
                            }
                        }

                        if(level == 4){
                          
                            if(this.value > highestScore){
                                highScoresRef.set({
                                    highestScoreLevel4 : this.value
                                }, { merge: true });
                            }
                        }

                        if(level == 5){
                            

                            if(this.value > highestScore){
                                highScoresRef.set({
                                    highestScoreLevel5 : this.value
                                }, { merge: true });
                            }
                        }
                                     
        
                    
              
            }
            this.scoreSend = true;
        }
        }
    }
}