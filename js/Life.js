class Life extends GameObject{

    constructor(intervalTime){
        super(intervalTime)
        this.count = 1;
        this.x = 0.9 * canvas.width;
        this.y = 0.08 * canvas.height;
    }

    addLife(){
        this.count++;
    }

    removeLife(){
        this.count--;
        if(this.count == 0){
            gameObjects[GAME_OVER_MESSAGE].show();
            gameObjects[ENEMY_BALL].moving = false;
            gameObjects[BALL].moving = false;
        }
    }

    render(){
        ctx.strokeStyle = "blue";
        ctx.font = "30px Arial";
        ctx.lineWidth = 3;
        ctx.strokeText(this.count,  this.x, this.y);
    }

}