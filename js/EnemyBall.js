/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland. */

class EnemyBall extends GameObject
{
    /* Each gameObject MUST have a constructor() and a render() method.        */
    /* If the object animates, then it must also have an updateState() method. */

    constructor(image, width, height, speed, intervalTime)
    {
        super(intervalTime); /* as this class extends from GameObject, you must always call super() */

        /* These variables depend on the object */
        this.image = image;
        
    
        this.width = width;
        this.height = height;
        this.x = Math.randomBetween(0, canvas.width - width); 
        this.y = Math.randomBetween(0, canvas.height - height);      
        this.stepSizeY = speed; 
        this.stepSizeX = speed; 
        this.moving  = true;
        }

    updateState()
    {
        //this.boundaryCheck();
        if(this.moving){
            this.x = this.x + this.stepSizeX;
            this.y = this.y + this.stepSizeY;
        }
        
    }

    render()
    {
        ctx.drawImage(this.image, this.x , this.y, this.width, this.height);
    }

    boundaryCheck(){
        if( this.x + this.width >= canvas.width ||  this.x <= 0){
            this.stepSizeX = -this.stepSizeX;
        } 

        if( this.y + this.height >= canvas.height ||  this.y <= 0){
            this.stepSizeY = -this.stepSizeY;
        } 
    }
}